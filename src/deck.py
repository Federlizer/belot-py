import random

class Card(object):
    RANKS = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']

    SUITS = ['♠', '♥', '♦', '♣']
    SUIT_NAMES = ['Clubs', 'Hearts', 'Diamonds', 'Spades']

    def __init__(self, rank, suit):
        if rank not in self.RANKS:
            raise ValueError("{} is not a valid rank".format(rank))
        if suit not in self.SUITS:
            raise ValueError("{} is not a valid suit".format(suit))

        self.rank = rank
        self.suit = suit

    def __str__(self):
        return '{}{}'.format(self.rank, self.suit)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.rank == other.rank and self.suit == other.suit

    def toJSON(self):
        return {
            "rank": self.rank,
            "suit": self.suit,
        }


def fresh_deck(exclude_ranks=[]):
    cards = []

    for suit in Card.SUITS:
        suit_cards = [ Card(rank, suit) for rank in Card.RANKS if rank not in exclude_ranks]
        cards += suit_cards

    return cards


class Deck(object):
    def __init__(self, shuffle=False, exclude_ranks=[]):
        self.cards = fresh_deck(exclude_ranks=exclude_ranks)

        if shuffle:
            self.shuffle()

    def __len__(self) -> int:
        return len(self.cards)

    def toJSON(self):
        return [c.toJSON() for c in self.cards]
        
    def shuffle(self):
        shuffled_cards = self.cards.copy()

        i = len(shuffled_cards)

        while i > 0:
            i -= 1
            r = random.randint(0, i)

            temp = shuffled_cards[i]
            shuffled_cards[i] = shuffled_cards[r]
            shuffled_cards[r] = temp
        
        self.cards = shuffled_cards

    def deal_card_to(self, card, player):
        player.give_card(card)

        self.cards = [sc for sc in self.cards if card != sc]
