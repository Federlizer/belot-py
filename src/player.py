from deck import Deck, Card

class Player(object):
    def __init__(self, username, hand, sid):
        self.username = username
        self.hand = hand
        self.sid = sid

    def __eq__(self, other):
        if type(other) != Player:
            return False
        return self.username == other.username and self.sid == other.sid

    def give_card(self, card):
        self.hand.append(card)

    def show_hand(self):
        for card in self.hand:
            print(card)

    def play_card(self, card):
        if type(card) != Card:
            raise TypeError("Excpet card to be of type {}".format(Card))

        index = self.hand.index(card)
        return self.hand.pop(index)

    def card_in_hand(self, card):
        if type(card) != Card:
            raise TypeError("Excpet card to be of type {}".format(Card))

        try:
            # no need to keep the index, just execute it in
            # order to get ValueError if it doesn't exist
            self.hand.index(card)
        except ValueError as e:
            print(e)
            return False

        # otherwise, it's there :)
        return True

    def hand_empty(self):
        return True if len(self.hand) == 0 else False

    def toJSON(self, hand=False, hand_length=False):
        """
        Returns a "ready-to-json-serialize" dictionary. The kwargs of this method define
        which fields (apart from the default ones) should be present in the returned dict.

        Default values (always included):
        - username
        - sid

        Possible to include:
        - hand: the exact hand of the player (rank, suit)
        - hand_length: the amount of cards held by the player
        """
        ret = {
            "username": self.username,
            "sid": self.sid,
        }

        if hand:
            ret["hand"] = [c.toJSON() for c in self.hand]

        if hand_length:
            ret["hand_length"] = len(self.hand)

        return ret