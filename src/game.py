from deck import Deck
from player import Player

def filter_belot_deck(full_deck):
    included_ranks = [ '7', '8', '9', '10', 'J', 'Q', 'K', 'A' ]

    return [ card for card in full_deck.cards if card in included_ranks ]


class Game:
    excluded_ranks = [ '2', '3', '4', '5', '6' ]

    def __init__(self, players):
        if len(players) != 4:
            raise ValueError("Can only play with 4 players")

        self.players = players
        self.team_one = (self.players[0], self.players[1])
        self.team_two = (self.players[2], self.players[3])

        self.deck = Deck(shuffle=True, exclude_ranks=Game.excluded_ranks)

        print(self.deck.cards)

        self.deal_hands()

    def deal_hands(self):
        i = 0

        for card in self.deck.cards:
            self.deck.deal_card_to(card, self.players[i])
            print(len(self.deck))

            i = 0 if i == len(self.players)-1 else i + 1

    def play(self):
        playing = True

        while playing:
            if all([player.hand_empty() for player in self.players]):
                playing = False
                continue

            for player in self.players:
                card_played = player.play_card()
                print('{} played {}'.format(player.name, card_played))

        print('Game over')
