from player import Player
from deck import Deck
from game import Game

players = [
    Player('Federlizer', []),
    Player('Sholta', []),
    Player('DAnonymousT', []),
    Player('SukiHan', []),
]

game = Game(players)

game.play()