from games.actions import PlayCardAction

from deck import Deck, Card
from player import Player


class HighCardWins:
    """
    events:
    - update_players: update all player info with what has been provided by the server
    - game_start: game has been started

    - action_request: an action has been requested by one of the players. The action info is provided with the event data

    - card_played: a card has been played by one of the players
    - hand_taken: the hand that's on the playboard has been taken by one of the players

    - update_scoreboard: asks the players to update their scoreboard
    """
    REQUIRED_PLAYER_AMOUNT = 4
    MAX_PLAYER_AMOUNT = 4

    # ordered highest to lowest
    RANK_POWER = ["A", "K", "Q", "J", "10", "9", "8", "7", "6", "5", "4", "3", "2"]
    RANK_POINTS = {
        "A":  14,
        "K":  13,
        "Q":  12,
        "J":  11,
        "10": 10,
        "9":  9,
        "8":  8,
        "7":  7,
        "6":  6,
        "5":  5,
        "4":  4,
        "3":  3,
        "2":  2,
    }

    def __init__(self):
        self._playing = False
        self._next_action = None
        self._players = []
        self._deck = Deck()
        self._board = [] # what cards are down on the board (by which players)
        self._scoreboard = {}

    @property
    def next_action(self):
        return self._next_action.toJSON()

    @property
    def scoreboard(self):
        return self._scoreboard

    @property
    def players(self):
        return self._players

    def add_player(self, player):
        """
        Returns a boolean indicating if the player was added.
        Cannot add more than 4 players to the game lobby.
        """
        if type(player) != Player:
            raise TypeError("player argument must be of type {}".format(Player))

        if len(self._players) >= self.MAX_PLAYER_AMOUNT:
            return False

        self._players.append(player)
        return True

    def remove_player(self, player):
        if type(player) != Player:
            raise TypeError("player argument must be of type {}".format(Player))

        index = self._players.index(player)
        return self._players.pop(index)

    def deal_hands(self):
        # shuffle deck first!
        self._deck.shuffle()

        # TODO this can probably be done better by using enumerate and subtraction/division
        i = 0
        for card in self._deck.cards:
            self._deck.deal_card_to(card, self._players[i])
            i = 0 if i == len(self._players)-1 else i + 1

    def start_game(self):
        if self._playing:
            raise Exception("You're already playing, can't start again")

        if len(self._players) < self.REQUIRED_PLAYER_AMOUNT:
            raise Exception("Not enough players have been added to the game")

        # setup scoreboard
        self._scoreboard = { player.username: {"hands": 0, "points": 0} for player in self._players }
        print("Setup scoreboard: {}".format(self._scoreboard))

        print("Dealing hands to players")
        self.deal_hands()

        self._next_action = self._resolve_next_action()

        print("Starting game")
        self._playing = True

    def stop_game(self):
        print("THEY WANT TO STOP THE GAME! Can we?: {}".format(self._playing))
        if not self._playing:
            raise Exception("You haven't started the game, can't reset game")

        # reset state
        self._deck = Deck()
        self._playing = False

    def play_action(self, action):
        """
        returns a tuple with a boolean if the action was completed, and the outcome of that action
        (action_successful, outcome)
        """

        # is it the correct action?
        if type(action) != type(self._next_action):
            print("Incorrect action type: {}".format(action))
            return False, None

        if not self._next_action.is_legal_action(action):
            print("Illegal action, skipping")
            return False, None

        # outcomes: "event_name": { .. event data .. }
        outcomes = {}
        force_next_player = None
        if type(action) == PlayCardAction:
            # play the card and resolve any outcomes
            # TODO: look at the execute_action docstring, you have stuff to do there
            card_played = action.execute_action()

            outcomes["card_played"] = {
                "card": card_played.toJSON(),
                "player": self._next_action.player.toJSON(),
            }

            self._board.append({
                "card": card_played,
                "player": self._next_action.player,
            })

            if len(self._board) == 4:
                player_that_took = self._resolve_hand_taker()
                force_next_player = player_that_took
                cards_taken = [c["card"] for c in self._board]

                self._update_scoreboard(player_that_took, cards_taken)
                self._board = []

                outcomes["hand_taken"] = {
                    "player": player_that_took.toJSON(),
                }

                outcomes["update_scoreboard"] = self.scoreboard
        else:
            raise TypeError("Action passed type check, but still couldn't be handled by the game")

        # resolve next action
        next_action = self._resolve_next_action(enforce_player=force_next_player)
        print("Next action: {}".format(next_action))
        self._next_action = next_action

        return True, outcomes

    def _update_scoreboard(self, player, cards_taken):
        """
        Updates the scoreboard. Gives points to the passed player with the total amount of points for the cards passed.
        """
        total_points = 0

        for card in cards_taken:
            points_for_card = self.RANK_POINTS[card.rank]
            total_points += points_for_card

        self._scoreboard[player.username]["points"] += total_points
        self._scoreboard[player.username]["hands"] += 1

    def _resolve_hand_taker(self):
        """
        Uses the board to resolve who takes the hand. Does not manipulate any game states.
        """
        wanted_suit = self._board[0]["card"].suit
        highest_rank = self._board[0]["card"].rank

        player_that_took = self._board[0]["player"]

        for board_action in self._board:
            card = board_action["card"]

            if card.suit == wanted_suit:
                current_highest_rank_index = self.RANK_POWER.index(highest_rank)
                opposing_rank_index = self.RANK_POWER.index(card.rank)

                # lower index means higher in power
                if opposing_rank_index < current_highest_rank_index:
                    highest_rank = card.rank
                    player_that_took = board_action["player"]

        return player_that_took

    def _resolve_next_action(self, enforce_player=None):
        """
        Figures out what's the next action and by which player. Player can be enforced by passing
        the enforce_player kwarg.

        This function will not manipulate any of the class's properties, instead will return the
        next action and it's the caller's job to resolve how to save this on the class's state.
        """
        if enforce_player:
            return PlayCardAction(enforce_player, requirements={})

        if self._next_action == None:
            return PlayCardAction(self._players[0], requirements={})

        last_player = None
        for player in self._players:
            if player == self._next_action.player:
                last_player = player
                break

        if not last_player:
            raise Exception("Couldn't find last playing player: {}".format(self._next_action))

        last_player_index = self._players.index(last_player)
        next_player_index = last_player_index+1 if last_player_index < len(self._players)-1 else 0

        return PlayCardAction(self._players[next_player_index], requirements={})