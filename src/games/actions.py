from player import Player
from deck import Card

def action_factory(action_name, player, payload):
    if action_name == "PLAY_CARD":
        return PlayCardAction(player, payload=payload)
    else:
        raise ValueError("Unknown action name {}".format(action_name))

class PlayCardAction(object):
    """
    This class can be used both to specify the next required action by a game
    or to hold the payload of an action that is played by a player and then checked
    against the actual requirements of the action stored in the game class.

    Player argument must be either the player that is supposed to play the action
    (when the action is created and stored in the game class) or the player
    that had played the action (when receiving an event to play an action from
    the clients)

    requirements:
        suit: string
        rank_higher_than: string (??)

    payload:
        rank: string
        suit: string
    """
    NAME = "PLAY_CARD"

    def __init__(self, player, requirements=None, payload=None):
        if requirements == None and payload == None:
            raise ValueError("You must specify the requirements or payload of this action")

        self.player = player
        self.requirements = requirements
        self.payload = payload

    def toJSON(self):
        return {
            "name": self.NAME,
            "player": self.player.toJSON(),
            "requirements": self.requirements,
        }

    def execute_action(self):
        """
        Executes the action with the provided (self) payload.
        NOTE: This method doesn't check against requirements, one will need
        to do the check beforehand with a *SAFE* requirements action.

        TODO: make this more generic, right now it returns the played card, but that's only
        a valid return value for this specific action, what about the call suit action?
        This doesn't even get rid of the card, only the upper method needs to do that..!?
        """

        if self.payload == None:
            raise Exception("Payload must be specified")

        suit = self.payload.get("suit", None)
        rank = self.payload.get("rank", None)

        if not rank or not suit:
            raise Exception("Invalid payload")

        card_played = Card(rank, suit)
        return self.player.play_card(card_played)

    def is_legal_action(self, action_played):
        """
        Checks if another action (of the same type) fulfills the requirements
        of this (self) action.
        """

        if self.requirements == None:
            raise Exception("Requirements for game stored actions must be specified when creating the action instance")
        if action_played.payload == None:
            raise Exception("Payload of played action must be specified when creating the action instance")

        # check player
        if self.player != action_played.player:
            print("Not {} player's turn".format(action_played.player))
            return False

        # is the card played in the player's hand?
        suit_played = action_played.payload.get("suit", None)
        rank_played = action_played.payload.get("rank", None)
        if not suit_played or not rank_played:
            print("Invalid action payload: {}".format(action_played.payload))
            return False
        card_played = Card(rank_played, suit_played)
        if not self.player.card_in_hand(card_played):
            print("Player doesn't have card {} in hand".format(card_played))
            return False

        # check suit
        if self.requirements.get("suit", None):
            if self.requirements["suit"] != card_played.suit:
                print("Player must play suit {} but played {}".format(self.requirements["suit"], card_played))
                return False

        return True