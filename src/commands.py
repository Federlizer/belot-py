from abc import ABC, abstractmethod


class CommandABC(ABC):
    @abstractmethod
    def run(self):
        raise NotImplementedError()


class RequestToJoin(CommandABC):
    def __init__(self, payload):
        if type(payload) != dict:
            raise TypeError("Payload must be of type dict")

        if not payload.get("name", None):
            raise ValueError("Payload must have a 'name' field")

        self.payload = payload

    def run(self):
        # we need a way to see the current players
        # is there enough space for the new player? rest assumes there is
        # add player to active players, return all players info
        # mention it to everyone else as well

        print("Running command with payload {}".format(self.payload))