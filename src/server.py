from flask import Flask, session, request
from flask_socketio import SocketIO, send, emit, join_room, leave_room
from flask_session import Session

from player import Player
from deck import Card
from games.actions import action_factory
from games.high_card_wins import HighCardWins

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config["SESSION_TYPE"] = "filesystem"

Session(app)

socketio = SocketIO(app, cors_allowed_origins="*")

game = HighCardWins()

@socketio.on('connect')
def handle_connect(*args, **kwargs):
    print("{} CONNECTED".format(request.sid))


@socketio.on("disconnect")
def handle_disconnect(*args, **kwargs):
    for player in game.players:
        if player.sid == request.sid:
            print("Removing {} from game lobby".format(player.username))
            game.remove_player(player)
            break

    print("{} DISCONNECTED".format(request.sid))

    if len(game.players) == 0:
        game.stop_game()


@socketio.on("play_action")
def handle_play_action(action):
    print("Handling a player plaing an action")
    print(action)

    action_name = action.get("name", None)
    payload = action.get("payload", None)
    player = None
    for p in game.players:
        if p.sid == request.sid:
            player = p

    if player == None:
        print("Couldn't find player with sid {} in current game".format(request.sid))

    if not action_name or not payload:
        print("Received invalid action {}".format(action))
        return

    action_obj = action_factory(action_name, player, payload)
    action_successful, outcomes = game.play_action(action_obj)

    if not action_successful:
        print("Action was unsucessful for some reason, look at previous messages to find out why")
        return

    print("Emitting outcomes: {}".format(outcomes))

    for event_name, value in outcomes.items():
        emit(event_name, value, to='game_room')

    update_client_game_state()
    emit("action_request", game.next_action, to="game_room")


@socketio.on('message')
def handle_message(command, payload):
    # ensure that arguments passed are in the correct format
    if type(command) != str:
        error_str = "Can't accept non-string command: {}".format(command)
        print(error_str)
        return error_str

    if type(payload) != dict:
        error_str = "Can't accept non-dictionary payload: {}".format(payload)
        print(error_str)
        return error_str

    # figure out which function to execute based on the command
    if command == "request_to_join":
        print("Running request_to_join command...")

        # check payload, must have all necessary parameters
        if not payload.get("username", None):
            return "Payload must have a 'username' field"

        # have they already joined or join with an already existing username?
        for player in game.players:
            if player.sid == request.sid:
                return "You've already joined the game, fuck you"
            if player.username == payload["username"]:
                return "This username is already taken, please try another one"

        # is there enough space for the new player?
        if len(game.players) >= game.MAX_PLAYER_AMOUNT:
            return "Not enough space"

        # add player to active players, return all players info
        print("{} joining game".format(payload["username"]))
        player = Player(payload["username"], [], request.sid)
        game.add_player(player)
        join_room('game_room')

        update_client_game_state()

        if len(game.players) == game.REQUIRED_PLAYER_AMOUNT:
            socketio.start_background_task(start_game)

        return True

    else:
        print("Unsupported command: {}".format(command))


def start_game():
    print("Going to start game!!!")

    with app.app_context():
        game.start_game()
        update_client_game_state(include_scoreboard=True)

        # announce game start
        emit("game_start", to="game_room", namespace="/")
        emit("action_request", game.next_action, to="game_room", namespace="/")


def update_client_game_state(include_scoreboard=False):
    print("Sending an update for the game state to all clients")

    with app.app_context():
        for player in game.players:
            data = []

            for p in game.players:
                json_args = {
                    "hand_length": True,
                    "hand": True if p.sid == player.sid else False,
                }
                data.append(p.toJSON(**json_args))

            emit("update_players", data, to=player.sid, namespace="/")

        if include_scoreboard:
            emit("update_scoreboard", game.scoreboard, to="game_room", namespace="/")


if __name__ == '__main__':
    socketio.run(app)
