export class SuitDoesNotExistError extends Error {
  constructor(suit: any) {
    super(`Suit ${suit} doesn't exist in this game.`);
  }
}

export class RankDoesNotExistError extends Error {
  constructor(rank: any) {
    super(`Rank ${rank} doesn't exist in this game.`);
  }
}

export class Card {
  static RANKS: string[] = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
  static SUITS = ['Spades', 'Hearts', 'Diamonds', 'Clubs'];

  static getSuitSymbol(suit: string) {
    const suitSymbolMap: {[suitName: string]: string} = {
      'Spades': '♠',
      'Hearts': '♥',
      'Diamonds': '♦',
      'Clubs': '♣',
    };

    const found = Object.keys(suitSymbolMap).find((s) => s === suit);
    if (!found) {
      throw new SuitDoesNotExistError(suit);
    }

    return suitSymbolMap[found];
  }

  // Instance definitions start here
  rank: string;
  suit: string;

  constructor(rank: string, suit: string) {
    const rankFound = Card.RANKS.find((r) => r === rank);
    const suitFound = Card.SUITS.find((s) => s === suit);

    if (!rankFound) {
      throw new RankDoesNotExistError(rank);
    }

    if (!suitFound) {
      throw new SuitDoesNotExistError(suit);
    }

    this.rank = rank;
    this.suit = suit;
  }

  getSuitSymbol() {
    return Card.getSuitSymbol(this.suit);
  }
}

export const freshDeck = (): Card[] => {
  const deck: Card[] = Card.SUITS.flatMap((suit) => {
    return Card.RANKS.map((rank) => {
      return new Card(rank, suit);
    });
  });

  return deck;
}

interface DeckProps {
  cards?: Card[];
  shuffle?: boolean;
}

export class Deck {
  cards: Card[];

  constructor(props: DeckProps) {
    if (!props.cards) {
      //1this.cards = freshDeck();
      this.cards = [];
    } else {
      this.cards = [ ...props.cards ];
    }
  }
}