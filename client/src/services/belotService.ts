import { Socket, io } from "socket.io-client";

export class ServiceAlreadyConnectedError extends Error {
  constructor() {
    super("This service is already connected.");
  }
}

export class BelotService {
  url: string;
  socket: Socket;

  constructor(url: string, connect = false) {
    this.url = url;
    this.socket = io(this.url, {
      autoConnect: connect,
    });
  }

  // WRAPPER METHODS
  async connect(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (this.socket.connected) {
        return resolve();
      }

      const resolveWrapper = () => {
        console.log("I'm still alive! But I shouldn't be...");
        this.socket.off('connect', resolveWrapper);
        resolve();
      }

      this.socket.on('connect', resolveWrapper);
      this.socket.connect();
    });
  }

  async disconnect(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (this.socket.disconnected) {
        return resolve();
      }

      const resolveWrapper = () => {
        console.log("Disconnect me is still alive, but it shouldn't be");
        this.socket.off('disconnect', resolveWrapper);
        resolve();
      }

      this.socket.on('disconnect', resolveWrapper);
      this.socket.disconnect();
    })
  }

  on(eventName: string, listener: (...args: any[]) => void): void {
    this.socket.on(eventName, listener);
  }

  off(eventName?: string|undefined, listener?: ((...args: any[]) => void) | undefined): void {
    this.socket.off(eventName, listener);
  }

  emit(ev: string, ...args: any[]) {
    this.socket.emit(ev, ...args)
  }

  // INSTANCE METHODS

  async requestToJoin(payload: object) {
    return new Promise<void>((resolve, reject) => {
      this.socket.emit('message', 'request_to_join', payload, (res: any) => {
        if (typeof(res) === 'string') {
          reject(res);
          return;
        }

        resolve();
      });
    });
  }
}