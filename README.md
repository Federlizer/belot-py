# Belot (but not really...)

Next steps:
- Setup a scoreboard, have that scoreboard be updated by the game server and clients can listen and update accordingly
- The scores should (probably) be based on card points. 2 gives 2, 10 gives 10, J gives 11, K gives 13, A gives 14.
- Enforce suit answering. If players can answer the suit, they should, otherwise, they can play whatever card.